from ldap3 import Server, Connection, ALL
import pymysql
import random, string

LDAP_HOST = "xrcb-ldap"
LDAP_USER_DN = "cn=admin,dc=xrcb,dc=cat"
LDAP_USER_PASSWORD = "payfornothing"
USER_OU = "ou=usuaris,dc=xrcb,dc=cat"

MYSQL_HOST = "xrcb-wordpress"
WORDPRESS_DB = "wordpress"
MYSQL_USER = "root"
MYSQL_USER_PASSWORD = "1234"

# import wp_users into LDAP
LDAP_IMPORT = False
# modify wordpress database to enable ldap logins
WORDPRESS_UPDATE = True


mysql_conn = pymysql.connect(host=MYSQL_HOST, user=MYSQL_USER, passwd=MYSQL_USER_PASSWORD, db=WORDPRESS_DB)
cursor1 = mysql_conn.cursor()

if WORDPRESS_UPDATE:
    # remove all wp_users' LDAP meta 
    cursor1.execute("DELETE from wp_usermeta where meta_key = 'authLDAP'")
    
    # create authLDAP meta for all wp_users
    cursor1.execute("SELECT ID, user_login FROM wp_users")
    cursor2 = mysql_conn.cursor()
    cursor2.execute("set autocommit = 1")
    for wp_user in cursor1:
        print(wp_user[1])
        cursor2.execute("INSERT INTO wp_usermeta (user_id, meta_key, meta_value) VALUES (%s, 'authLDAP', 1)" % wp_user[0])
    cursor2.close()
    

if LDAP_IMPORT:
    s = Server(LDAP_HOST, port=389, get_info=ALL)
    ldap_conn = Connection(s, user=LDAP_USER_DN, password=LDAP_USER_PASSWORD, check_names=True, lazy=False, raise_exceptions=True)
    ldap_conn.bind()
    cursor1.execute("SELECT user_login, user_email FROM wp_users")
    for wp_user in cursor1:
        print(wp_user[0])
        rand_str = lambda n: ''.join([random.choice(string.ascii_lowercase) for i in range(n)])
        ldap_conn.add('uid=%s,%s' % (wp_user[0],USER_OU), 'inetOrgPerson', {'cn': wp_user[0], 'sn': 'Surname', 'userPassword': rand_str(16), 'mail': wp_user[1]})
    ldap_conn.unbind()


cursor1.close()
mysql_conn.close()
